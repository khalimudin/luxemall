part of 'services.dart';

class CartServices {
  static Future<bool> addToCart(int productId, int quantity,
      {http.Client client}) async {
    String url = 'https://fakestoreapi.com/carts';

    client ??= http.Client();

    final df = new DateFormat('yyyy-mm-dd');

    final userId = 1; // hardcode userId

    var response = await client.post(url,
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          'userId': userId,
          'date': df.format(new DateTime.now()),
          'products': [
            {'productId': productId, 'quantity': quantity},
          ]
        }));

    var result = json.decode(response.body);

    if (response.statusCode == 200 && result['userId'] == userId) return true;

    return false;
  }
}
