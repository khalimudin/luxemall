part of 'services.dart';

class ProductServices {
  static Future<List<Product>> getProducts(int page, String category,
      {String sort = "asc", http.Client client}) async {
    int perPage = 4;
    var limit = page * perPage;
    String url = 'https://fakestoreapi.com/products?limit=$limit&sort=$sort';

    if (category != 'all') {
      url =
          'https://fakestoreapi.com/products/category/$category?limit=$limit&sort=$sort';
    }

    client ??= http.Client();

    var response = await client.get(url);

    if (response.statusCode != 200) {
      return [];
    }
    var result = json.decode(response.body) as List;
    int minIndex = (page - 1) * perPage;
    if (minIndex >= result.length) {
      return [];
    }

    return result
        .sublist(minIndex, min(result.length, page * perPage))
        .map((e) => Product.fromJson(e))
        .toList();
  }

  static Future<Product> getProductById(int id, {http.Client client}) async {
    String url = 'https://fakestoreapi.com/products/$id';

    client ??= http.Client();

    var response = await client.get(url);

    var result = json.decode(response.body);

    return Product.fromJson(result);
  }
}
