part of 'models.dart';

class Product extends Equatable {
  final int id;
  final String title;
  final double price;
  final String category;
  final String description;
  final String image;

  Product({
    @required this.id,
    @required this.title,
    @required this.price,
    @required this.category,
    @required this.description,
    @required this.image,
  });

  factory Product.fromJson(Map<String, dynamic> json) => Product(
        id: json["id"],
        title: json["title"],
        price: json["price"].toDouble(),
        category: json["category"],
        description: json["description"],
        image: json["image"],
      );

  @override
  List<Object> get props => [id, title, price, category, description, image];

  @override
  String toString() {
    return 'id: $id, title: $title, category: $category';
  }
}
