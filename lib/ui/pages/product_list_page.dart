part of 'pages.dart';

class ProductListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CategoryBloc>(
          create: (context) => CategoryBloc(),
        ),
        BlocProvider<SortBloc>(
          create: (context) => SortBloc(),
        ),
        BlocProvider<ProductBloc>(
          create: (context) => ProductBloc(
              catBloc: BlocProvider.of<CategoryBloc>(context),
              sortBloc: BlocProvider.of<SortBloc>(context))
            ..add(GetProductsEvent("all")),
        ),
      ],
      child: Scaffold(
        appBar: CustomAppBar(),
        backgroundColor: Colors.white,
        body: SafeArea(
          child: ProductList(),
        ),
      ),
    );
  }
}

class ProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var categories = [
      'all',
      "electronics",
      "jewelery",
      "men clothing",
      "women clothing"
    ];

    ProductBloc _productBloc = BlocProvider.of<ProductBloc>(context);
    CategoryBloc _catBloc = BlocProvider.of<CategoryBloc>(context);
    SortBloc _sortBloc = BlocProvider.of<SortBloc>(context);

    return ListView(
      children: [
        Column(
          children: [
            BlocBuilder<CategoryBloc, CategoryState>(
              bloc: _catBloc,
              builder: (context, state) {
                return Container(
                  height: 64,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        mainAxisSize: MainAxisSize.max,
                        children: categories.map((cat) {
                          return Padding(
                            padding: const EdgeInsets.only(right: 4.0),
                            child: ChoiceChip(
                              label: Text(cat),
                              selected: state.category == cat,
                              onSelected: (bool selected) {
                                var _choosedCategory = selected ? cat : 'all';
                                _catBloc
                                    .add(ChangeCategoryEvent(_choosedCategory));
                              },
                            ),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                );
              },
            ),
            BlocBuilder<ProductBloc, ProductState>(
              bloc: _productBloc,
              builder: (context, state) {
                var defaultMargin = 16.0;
                if (state is ProductInitial) {
                  return Center(
                    child: Loading(),
                  );
                }
                if (state is ProductError) {
                  return Center(
                    child: Text('failed to fetch products'),
                  );
                }
                if (state is ProductLoaded) {
                  if (state.products.isEmpty) {
                    return Center(
                      child: Text('no product'),
                    );
                  }
                  return GridView.builder(
                    padding: EdgeInsets.all(8),
                    physics: ScrollPhysics(),
                    shrinkWrap: true,
                    // Need to display a loading tile if more items are coming
                    itemCount: !state.hasReachedMax
                        ? state.products.length + 2
                        : state.products.length,

                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 9 / 16,
                      crossAxisSpacing: 1 / 2 * defaultMargin,
                      mainAxisSpacing: defaultMargin,
                    ),
                    itemBuilder: (BuildContext context, int index) {
                      if (index >= state.products.length) {
                        // Don't trigger if one async loading is already under way
                        if (!state.isLoading) {
                          _productBloc
                              .add(GetProductsEvent(_catBloc.state.category));
                        }
                        return Center(
                          child: Loading(),
                        );
                      }

                      final itemWidth = (MediaQuery.of(context).size.width -
                              2.5 * defaultMargin) /
                          2;

                      return ProductCard(
                        width: itemWidth,
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => ProductDetailPage(
                                  id: state.products[index].id),
                            ),
                          );
                        },
                        product: state.products[index],
                      );
                    },
                  );
                }
                return null;
              },
            ),
          ],
        ),
      ],
    );
  }
}

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  CustomAppBar({Key key})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize;

  @override
  Widget build(BuildContext context) {
    SortBloc _sortBloc = BlocProvider.of<SortBloc>(context);

    return AppBar(
      leading: Icon(Icons.home),
      title: Text('Luxemall'),
      centerTitle: true,
      actions: [
        BlocBuilder<SortBloc, SortState>(
          bloc: _sortBloc,
          builder: (context, state) {
            return PopupMenuButton<String>(
              icon: Icon(Icons.sort),
              onSelected: (value) {
                _sortBloc.add(ChangeSorting(value));
              },
              itemBuilder: (BuildContext context) {
                return {'asc', 'desc'}.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Radio(
                          groupValue: state.type,
                          value: choice,
                          onChanged: (String s) {},
                        ),
                        Text(choice)
                      ],
                    ),
                  );
                }).toList();
              },
            );
          },
        ),
      ],
    );
  }
}
