import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:luxemall/bloc/cart_bloc.dart';
import 'package:luxemall/bloc/category_bloc.dart';
import 'package:luxemall/bloc/product_bloc.dart';
import 'package:luxemall/bloc/single_product_bloc.dart';
import 'package:luxemall/bloc/sort_bloc.dart';
import 'package:luxemall/models/models.dart';
import 'package:luxemall/shared/constants.dart';
import 'package:luxemall/ui/widgets/widgets.dart';

part 'product_list_page.dart';
part 'product_detail_page.dart';
