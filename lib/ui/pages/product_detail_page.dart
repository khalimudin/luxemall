part of 'pages.dart';

class ProductDetailPage extends StatelessWidget {
  final int id;

  const ProductDetailPage({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Luxemall'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: BlocProvider(
            create: (context) => SingleProductBloc()..add(GetProduct(id)),
            child: ProductPage()),
      ),
    );
  }
}

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  SingleProductBloc _productBloc;

  @override
  void initState() {
    super.initState();
    _productBloc = BlocProvider.of<SingleProductBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SingleProductBloc, SingleProductState>(
      bloc: _productBloc,
      builder: (context, state) {
        var defaultMargin = 16.0;
        if (state is SingleProductInitial) {
          return Center(
            child: Loading(),
          );
        }
        if (state is SingleProductError) {
          return Center(
            child: Text('failed to fetch products'),
          );
        }
        if (state is SingleProductLoaded) {
          final itemWidth =
              (MediaQuery.of(context).size.width - 2 * defaultMargin);

          return Padding(
            padding: EdgeInsets.fromLTRB(
                defaultMargin, defaultMargin, defaultMargin, 2 * defaultMargin),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: ProductDetailCard(
                    width: itemWidth,
                    product: state.product,
                  ),
                ),
                SizedBox(height: 30),
                SizedBox(
                  width: double.infinity,
                  height: 64,
                  child: ElevatedButton(
                    onPressed: () {
                      showBottomSheet(
                        context: context,
                        builder: (context) => BottomSheetWidget(
                          child: BlocProvider(
                            create: (context) => CartBloc(),
                            child: AddToCartForm(
                              productId: state.product.id,
                            ),
                          ),
                        ),
                      );
                    },
                    child: Text(
                      "Add to Cart",
                      style: TextStyle(fontSize: 20.0),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: primaryColor, // background
                      onPrimary: Colors.white, // foreground
                    ),
                  ),
                ),
              ],
            ),
          );
        }
        return Container();
      },
    );
  }
}

class ProductDetailCard extends StatelessWidget {
  final Product product;
  final Function onTap;
  final double width;

  ProductDetailCard({
    this.product,
    this.onTap,
    this.width = 210,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) onTap();
      },
      child: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 12 / 9 * width,
              width: width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 6.0,
                  ),
                ],
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 0, 16.0, 32.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      product.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.w600,
                          fontSize: 24),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      '${product.category}',
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(color: Colors.grey, fontSize: 16),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Text(
                      '\$ ${product.price}',
                      style: TextStyle(
                          color: Colors.black87,
                          fontWeight: FontWeight.w600,
                          fontSize: 24.0),
                    ),
                    TextButton(
                      child: Text('more info'),
                      style: TextButton.styleFrom(primary: primaryColor),
                      onPressed: () {
                        showBottomSheet(
                          context: context,
                          builder: (context) => BottomSheetWidget(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(24, 8, 24, 8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: Text(
                                      'Description',
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 24,
                                      ),
                                    ),
                                  ),
                                  Text(
                                    product.description,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: 5 / 4 * width,
              width: 0.8 * width,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(product.image),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BottomSheetWidget extends StatelessWidget {
  final Widget child;

  const BottomSheetWidget({this.child});

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
        constraints: BoxConstraints(
          minHeight: 160,
        ),
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(blurRadius: 10, color: Color(0xFF585858), spreadRadius: 5)
          ]),
          child: child,
        ));
  }
}

class DecoratedTextField extends StatelessWidget {
  final int quantity;
  final Function onChange;

  const DecoratedTextField({Key key, this.quantity, this.onChange})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        alignment: Alignment.center,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        decoration: BoxDecoration(
            color: Colors.grey[300], borderRadius: BorderRadius.circular(10)),
        child: TextField(
          decoration: InputDecoration.collapsed(
            hintText: 'Enter quantity',
          ),
          keyboardType: TextInputType.number,
          onChanged: onChange,
        ));
  }
}

class AddCartButton extends StatelessWidget {
  final bool loading;
  final bool success;
  final Function onAdd;

  const AddCartButton({Key key, this.loading, this.success, this.onAdd})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : !success
            ? MaterialButton(
                color: primaryColor,
                onPressed: onAdd,
                child: Text(
                  'Add',
                  style: TextStyle(color: Colors.white),
                ),
              )
            : Icon(
                Icons.check,
                color: Colors.green,
              );
  }
}

class AddToCartForm extends StatefulWidget {
  final int productId;
  const AddToCartForm({Key key, this.productId}) : super(key: key);

  @override
  _AddToCartFormState createState() => _AddToCartFormState();
}

class _AddToCartFormState extends State<AddToCartForm> {
  int quantity = 0;

  @override
  Widget build(BuildContext context) {
    CartBloc cartBloc = BlocProvider.of<CartBloc>(context);

    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          height: 125,
          child: Column(
            children: [
              DecoratedTextField(
                onChange: onChangeQuantity,
                quantity: quantity,
              ),
              BlocBuilder<CartBloc, CartState>(
                bloc: cartBloc,
                builder: (context, state) {
                  return AddCartButton(
                    loading: state is CartLoading,
                    success: state is CartSuccess,
                    onAdd: () => onAdd(cartBloc),
                  );
                },
              ),
              BlocListener<CartBloc, CartState>(
                bloc: cartBloc,
                listener: (context, state) async {
                  if (state is CartError) {
                    final snackBar =
                        SnackBar(content: Text('Something Error, try again'));
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    return;
                  } else if (state is CartSuccess) {
                    await Future.delayed(Duration(milliseconds: 1000));
                    Navigator.pop(context);
                  }
                },
                child: Container(),
              ),
            ],
          ),
        )
      ],
    );
  }

  onChangeQuantity(val) => setState(() {
        quantity = int.tryParse(val) ?? 0;
        if (quantity < 0) quantity = 0;
      });

  onAdd(CartBloc cartBloc) async {
    final snackBar = SnackBar(content: Text('Quantity minimum 1'));

    if (quantity < 1) {
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return;
    }
    cartBloc.add(AddToCart(productId: widget.productId, quantity: quantity));
  }
}
