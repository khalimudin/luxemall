import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:luxemall/models/models.dart';
import 'package:luxemall/services/services.dart';

part 'single_product_event.dart';
part 'single_product_state.dart';

class SingleProductBloc extends Bloc<SingleProductEvent, SingleProductState> {
  SingleProductBloc() : super(SingleProductInitial());

  @override
  Stream<SingleProductState> mapEventToState(
    SingleProductEvent event,
  ) async* {
    if (event is GetProduct) {
      try {
        var product = await ProductServices.getProductById(event.id);
        yield SingleProductLoaded(product);
      } catch (_) {
        yield SingleProductError();
      }
    }
  }
}
