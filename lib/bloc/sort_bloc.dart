import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'sort_event.dart';
part 'sort_state.dart';

class SortBloc extends Bloc<SortEvent, SortState> {
  SortBloc() : super(SortState("asc"));

  @override
  Stream<SortState> mapEventToState(
    SortEvent event,
  ) async* {
    if (event is ChangeSorting) {
      yield SortState(event.type);
    }
  }
}
