import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:luxemall/bloc/category_bloc.dart';
import 'package:luxemall/bloc/sort_bloc.dart';
import 'package:luxemall/models/models.dart';
import 'package:luxemall/services/services.dart';

part 'product_event.dart';
part 'product_state.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  final CategoryBloc catBloc;
  final SortBloc sortBloc;

  ProductBloc({this.catBloc, this.sortBloc}) : super(ProductInitial()) {
    catBloc.stream.listen((state) {
      add(NewCategoryEvent(state.category));
    });

    sortBloc.stream.listen((state) {
      add(NewSortingEvent(state.type));
    });
  }

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    final currentState = state;
    final sort = sortBloc.state.type;
    if (event is GetProductsEvent && !_hasReachedMax(currentState)) {
      try {
        if (currentState is ProductInitial) {
          var products =
              await ProductServices.getProducts(1, event.category, sort: sort);
          yield ProductLoaded(
              products: products,
              hasReachedMax: false,
              isLoading: false,
              nextPage: 2);
          return;
        } else if (currentState is ProductLoaded) {
          yield currentState.copyWith(isLoading: true);
          var res = await ProductServices.getProducts(
              currentState.nextPage, event.category,
              sort: sort);
          if (res.isEmpty) {
            yield currentState.copyWith(hasReachedMax: true, isLoading: false);
          } else {
            yield ProductLoaded(
                products: currentState.products + res,
                hasReachedMax: false,
                isLoading: false,
                nextPage: currentState.nextPage + 1);
          }
        }
      } catch (_) {
        yield ProductError();
      }
    } else if (event is NewCategoryEvent) {
      yield ProductInitial();
      add(GetProductsEvent(event.category));
    } else if (event is NewSortingEvent) {
      yield ProductInitial();
      add(GetProductsEvent(catBloc.state.category));
    }
  }

  bool _hasReachedMax(ProductState state) =>
      state is ProductLoaded && state.hasReachedMax;
}
