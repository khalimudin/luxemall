part of 'product_bloc.dart';

abstract class ProductState extends Equatable {
  const ProductState();

  @override
  List<Object> get props => [];
}

class ProductInitial extends ProductState {}

class ProductError extends ProductState {}

class ProductLoaded extends ProductState {
  final List<Product> products;
  final bool hasReachedMax;
  final bool isLoading;
  final int nextPage;

  const ProductLoaded(
      {this.products, this.hasReachedMax, this.isLoading, this.nextPage = 1});

  ProductLoaded copyWith(
      {List<Product> products,
      bool hasReachedMax,
      bool isLoading,
      int nextPage}) {
    return ProductLoaded(
        products: products ?? this.products,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        isLoading: isLoading ?? this.isLoading,
        nextPage: nextPage ?? this.nextPage);
  }

  @override
  List<Object> get props => [products, hasReachedMax];

  @override
  String toString() =>
      'ProductLoaded { products: ${products.length}, hasReachedMax: $hasReachedMax }';
}
