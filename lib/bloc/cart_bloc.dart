import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:luxemall/services/services.dart';

part 'cart_event.dart';
part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  CartBloc() : super(CartInitial());

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    if (event is AddToCart) {
      yield CartLoading();
      try {
        if (await CartServices.addToCart(event.productId, event.quantity)) {
          yield CartSuccess();
        } else {
          yield CartError();
        }
      } catch (e) {
        yield CartError();
      }
    }
  }
}
