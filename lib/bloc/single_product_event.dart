part of 'single_product_bloc.dart';

abstract class SingleProductEvent extends Equatable {
  const SingleProductEvent();

  @override
  List<Object> get props => [];
}

class GetProduct extends SingleProductEvent {
  final int id;

  const GetProduct(this.id);

  @override
  List<Object> get props => [id];
}
