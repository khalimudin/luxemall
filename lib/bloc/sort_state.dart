part of 'sort_bloc.dart';

class SortState extends Equatable {
  final String type;
  const SortState(this.type);

  @override
  List<Object> get props => [type];
}
