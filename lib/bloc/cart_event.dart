part of 'cart_bloc.dart';

abstract class CartEvent extends Equatable {
  const CartEvent();

  @override
  List<Object> get props => [];
}

class AddToCart extends CartEvent {
  final int productId;
  final int quantity;

  const AddToCart({this.productId, this.quantity});

  @override
  List<Object> get props => [productId, quantity];
}
