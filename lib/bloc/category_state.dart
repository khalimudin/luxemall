part of 'category_bloc.dart';

class CategoryState extends Equatable {
  final String category;
  const CategoryState(this.category);

  @override
  List<Object> get props => [category];
}
