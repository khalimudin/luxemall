part of 'product_bloc.dart';

abstract class ProductEvent extends Equatable {
  const ProductEvent();

  @override
  List<Object> get props => [];
}

class GetProductsEvent extends ProductEvent {
  final String category;

  const GetProductsEvent(this.category);

  @override
  List<Object> get props => [category];
}

class NewCategoryEvent extends ProductEvent {
  final String category;

  const NewCategoryEvent(this.category);

  @override
  List<Object> get props => [category];
}

class NewSortingEvent extends ProductEvent {
  final String type;

  const NewSortingEvent(this.type);

  @override
  List<Object> get props => [type];
}
