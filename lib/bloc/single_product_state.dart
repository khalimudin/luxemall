part of 'single_product_bloc.dart';

abstract class SingleProductState extends Equatable {
  const SingleProductState();

  @override
  List<Object> get props => [];
}

class SingleProductInitial extends SingleProductState {}

class SingleProductError extends SingleProductState {}

class SingleProductLoaded extends SingleProductState {
  final Product product;

  const SingleProductLoaded(this.product);

  @override
  List<Object> get props => [product];
}
