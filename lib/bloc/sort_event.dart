part of 'sort_bloc.dart';

abstract class SortEvent extends Equatable {
  const SortEvent();

  @override
  List<Object> get props => [];
}

class ChangeSorting extends SortEvent {
  final String type;

  const ChangeSorting(this.type);

  @override
  List<Object> get props => [type];
}
